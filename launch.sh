!#/bin/bash

#update packages
yum update -y
yum upgrade -y

#install kernel modules
modprobe tun
modprobe ah4
modprobe esp4
modprobe xfrm4_tunnel
modprobe xfrm_user
modprobe xfrm4_mode_tunnel
modprobe pcrypt
modprobe xfrm_ipcomp
modprobe deflate

lsmod

